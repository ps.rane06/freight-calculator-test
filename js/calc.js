$(document).ready(function () {
  getItemsList();
  $("#itemSelect").on("change", function (e) {
    let [, bcd, igst] = $(this).val().split(',');
    if (!bcd || !igst) {
      $('#item_details_container').addClass('d-none');
    }
    else {
      $('#item_details_container').removeClass('d-none');
      $('#item_bcd').text(bcd);
      $('#item_igst').text(igst);
      $('#result_div').addClass('d-none');
    }
  });
});

async function getItemsList() {
  const response = await axios.post('./scripts/getItemsList.php');
  const itemsList = [response.data];
  let itemSelect = document.getElementById('itemSelect');
  itemsList[0].forEach(item => {
    const option = document.createElement('option');
    option.value = [item.cth_code, item.bcd, item.igst];
    option.text = item.name;
    option.bcd = item.bcd;
    option.igst = item.igst;
    itemSelect.appendChild(option);
  });

  $('#itemSelect').select2({
    placeholder: 'Select items',
    theme: "bootstrap"
  });
}

async function calculateTotalCost() {
  try {
    const invoiceValue = document.getElementById('invoice_value').value;
    const statedWeight = document.getElementById('stated_weight').value;
    const dimensionWeight = document.getElementById('dimension_weight').innerText;
    const bcd = document.getElementById('item_bcd').innerText;
    const igst = document.getElementById('item_igst').innerText;
    const exchange_rate = document.getElementById('exchange_rate').value;

    if (!invoiceValue || !statedWeight || !dimensionWeight || !exchange_rate
      || invoiceValue == 0 || statedWeight == 0 || dimensionWeight == 0 || exchange_rate == 0) {
      showError("All fields are required");
    }

    const response = await axios.post('./scripts/freight_calc.php', {
      invoiceValue,
      statedWeight,
      dimensionWeight,
      igst,
      bcd,
      exchange_rate,
      functionName: "calculateTotalCost"
    });

    // update values to the frontend
    updateFields(response);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

function updateFields(response) {
  document.getElementById('actual_weight').innerText = response.data.actual_weight;
  /* document.getElementById('c1').innerText = response.data.c1;
  document.getElementById('c2').innerText = response.data.c2; */
  document.getElementById('freight').innerText = response.data.freight;
  document.getElementById('insurance').innerText = response.data.insurance;
  document.getElementById('fob').innerText = response.data.fob;
  document.getElementById('landing_charges_fob').innerText = response.data.landing_charges_fob;
  document.getElementById('assessable_value_fob').innerText = response.data.assessable_value_fob;
  document.getElementById('cif_value').innerText = response.data.cif_value;
  document.getElementById('landing_charges_cif').innerText = response.data.landing_charges_cif;
  document.getElementById('assessable_value_cif').innerText = response.data.assessable_value_cif;
  document.getElementById('duty').innerText = response.data.duty;
  document.getElementById('sws').innerText = response.data.sws;
  document.getElementById('igst').innerText = response.data.calculated_igst;
  document.getElementById('total').innerText = response.data.total;
  document.getElementById('result_div').classList.remove('d-none');
}

function getDimensionValue() {
  const dimensionWeightFields = document.getElementById(
    "dimension_weight_fields"
  );
  let dimensionsArray = [];
  dimensionWeightFields.querySelectorAll("input").forEach((input) => {
    if (input.value == 0 || input.value == '') {
      showError("Height, width and lenght is required!");
    }
    dimensionsArray.push(input.value);
  });
  axios.post('./scripts/freight_calc.php', { dimensions: dimensionsArray, functionName: "getDimensionWeight" })
    .then((response) => {
      const dimensionWeight = document.getElementById('dimension_weight');
      dimensionWeight.parentElement.classList.remove('d-none');
      dimensionWeight.innerText = response.data;
      document.getElementById('btn_calculateTotalCost').classList.remove('d-none');
    }).catch((error) => {
      console.error("Error fetching data:", error);
    });
}

function getExchangeRate() {
  axios.post('./scripts/freight_calc.php', {
    functionName: "getExchangeRate"
  })
    .then((response) => {
      $('#exchange_rate').val(response.data);
    })
    .catch((e) => {
      throw new Error(e);
    })
}

function showError(msg) {
  // document.getElementById('error').innerText = msg;
  alert(msg);
  throw new Error(msg);
}