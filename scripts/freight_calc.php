<?php
$env = parse_ini_file('../.env');
// Retrieve the raw request body
$request_body = file_get_contents('php://input');

// Parse the JSON data
$data = json_decode($request_body, true);
$data = [...$data, $env];

$function_name = $data['functionName'] ?? '';

switch ($function_name) {
    case 'getDimensionWeight':
        echo getDimensionWeight($data);
        break;
    case 'calculateTotalCost':
        echo calculateTotalCost($data);
        break;
    case 'getExchangeRate':
        echo getExchangeRate($data);
        break;
    default:
        echo new Error("Function name missing");
        break;
}

function calculateTotalCost($data)
{
    try {
        $invoice_val = $data['invoiceValue'] ?? 0;
        $stated_weight = $data['statedWeight'] ?? 0;
        $dimension_weight = $data['dimensionWeight'] ?? 0;
        $bcd = $data['bcd'];
        $igst = $data['igst'];
        $exchange_rate = $data['exchange_rate'] ?? 0;

        $zone = getZone('US');
        $actual_weight = getActualWeight($dimension_weight, $stated_weight);

        $c1 = getC1($invoice_val, $exchange_rate);

        $c2 = getC2($actual_weight, $zone, $exchange_rate);

        $freight = getFreight($c1, $c2);

        $insurance = getInsurance($invoice_val, $exchange_rate);

        $fob = getFOB($invoice_val, $exchange_rate);

        $landing_charges_fob = getLandingChargesFOB($fob);

        $assessable_value_fob = getAssessableValueFOB($fob, $landing_charges_fob);

        $cif_value = getCIFValue($freight, $insurance, $invoice_val, $exchange_rate);

        $landing_charges_cif = getLandingChargesCIF($cif_value);

        $assessable_value_cif = getAssessableValueCIF($cif_value, $landing_charges_cif);

        $duty = getDuty($assessable_value_cif, $bcd);

        $sws = getSWS($duty);

        $calculated_igst = calculateIgst($assessable_value_cif, $sws, $duty, $igst);

        $total = getTotal($duty, $sws, $calculated_igst);

        return json_encode([
            'exchange_rate' => $exchange_rate,
            'zone' => $zone,
            'actual_weight' => $actual_weight,
            'c1' => $c1,
            'c2' => $c2,
            'freight' => $freight,
            'insurance' => $insurance,
            'fob' => $fob,
            'landing_charges_fob' => $landing_charges_fob,
            'assessable_value_fob' => $assessable_value_fob,
            'bcd' => $bcd,
            'igst' => $igst,
            'cif_value' => $cif_value,
            'landing_charges_cif' => $landing_charges_cif,
            'assessable_value_cif' => $assessable_value_cif,
            'duty' => $duty,
            'sws' => $sws,
            'calculated_igst' => $calculated_igst,
            'total' => $total
        ]);
    } catch (Exception $e) {
        echo new Error("Error occurred " . $e);
    }
}


function getActualWeight($dimension_weight, $stated_weight)
{
    return max($dimension_weight, $stated_weight);
}

function getDimensionWeight($data)
{
    [$h, $w, $l] = $data['dimensions'];
    return round((($h * $w * $l) / 5000), 6);
}

function getExchangeRate($data)
{
    $req_url = "https://v6.exchangerate-api.com/v6/" . $data['EXCHANGE_API_KEY'] . "/latest/USD";
    $response_json = file_get_contents($req_url);
    if (false !== $response_json) {
        try {
            $response = json_decode($response_json);
            if ('success' === $response->result) {
                return $USD_price = round(($response->conversion_rates->INR), 2);
            }

        } catch (Exception $e) {
            throw new ErrorException("error occured: " . $e);
        }
    }
}

function getZone($zone)
{
    return $zone === 'US' ? 2.92 : 3.92;
}

function getC1($inv_val, $ex_rate)
{
    return round((($inv_val * $ex_rate) * 20 / 100), 4);
}

function getC2($act_weight, $zone, $ex_rate)
{
    return round((($act_weight * $zone) * $ex_rate), 4);
}

function getFreight($c1, $c2)
{
    return min($c1, $c2);
}

function getInsurance($inv_val, $ex_rate)
{
    return round((($inv_val * $ex_rate) * 1.125 / 100), 2);
}

function getFOB($inv_val, $ex_rate)
{
    return round(($inv_val * $ex_rate), 4);
}

function getLandingChargesFOB($fob)
{
    return round(($fob / 100), 4);
}

function getAssessableValueFOB($fob, $landingCharges)
{
    return round(($fob + $landingCharges), 2);
}

function getCIFValue($freight, $insurance, $inv_val, $ex_rate)
{
    return round((($freight + $insurance) + ($inv_val * $ex_rate)), 4);
}

function getLandingChargesCIF($cif)
{
    return round(($cif / 100), 4);
}

function getAssessableValueCIF($cif, $landing_charge_cif)
{
    return round(($cif + $landing_charge_cif), 4);
}

function getDuty($av_cif, $bcd)
{
    return round((($av_cif * $bcd) / 100), 2);
}

function getSWS($duty)
{
    return round(($duty * 10 / 100), 2);
}

function calculateIgst($av_cif, $sws, $duty, $igst)
{
    return round((($av_cif + $sws + $duty) * $igst / 100), 2);
}

function getTotal($duty, $sws, $calc_igst)
{
    return round(($duty + $sws + $calc_igst), 4);
}