<?php
require_once __DIR__ . '../../db/db.php';

try {
    $env = parse_ini_file(__DIR__ . '../../.env');
    $conn = Database::getConnection($env);
    $sql = "SELECT cth_code, name, bcd, igst FROM items";
    $result = $conn->query($sql);

    $items = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $items[] = $row;
        }
    }
    echo json_encode($items);
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}