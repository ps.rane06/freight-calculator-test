-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2024 at 11:48 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freight_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(3) DEFAULT NULL,
  `name` varchar(186) DEFAULT NULL,
  `cth_code` int(8) DEFAULT NULL,
  `bcd` decimal(3,1) DEFAULT NULL,
  `igst` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `cth_code`, `bcd`, `igst`) VALUES
(1, 'Prepared food from Cereals', 19049000, 30.0, 18),
(2, 'Protein Concentrates', 21061000, 40.0, 18),
(3, 'Cat & Dog Food', 23091000, 30.0, 0),
(4, 'Preparations of minerals and their supplements', 30045020, 10.0, 12),
(5, 'Preparations of Vitamins', 30045039, 10.0, 12),
(6, 'Adhesive Tapes', 30051020, 10.0, 12),
(7, 'Bandages', 30059040, 10.0, 12),
(8, 'Callous Remover', 30059050, 10.0, 12),
(9, 'Printing Ink,Writing or Drawing and Other Inks', 32151190, 10.0, 12),
(10, 'Beauty or Make-Up Preparations', 33019090, 20.0, 18),
(11, 'PERFUMES AND TOILET WATERS', 33030010, 20.0, 18),
(12, 'Lip Make-up Prepartions', 33041000, 20.0, 18),
(13, 'Eye Make-Up Preparation', 33042000, 20.0, 18),
(14, 'Talcum Powder', 33049120, 20.0, 18),
(15, 'Face Creams', 33049910, 20.0, 18),
(16, 'Moisturising Cream', 33049930, 20.0, 18),
(17, 'Shampoo', 33051090, 20.0, 18),
(18, 'Hair Oil', 33059019, 20.0, 18),
(19, 'Hair Cream', 33059030, 20.0, 18),
(20, 'Hair Dyes', 33059040, 20.0, 18),
(21, 'Hair Regrowth', 33059090, 20.0, 18),
(22, 'Preparations for Oral or Dental Hygiene', 33061090, 20.0, 18),
(23, 'Dental Floss', 33069000, 20.0, 18),
(24, 'Pre-Shave, Shaving or After Shave Preparations', 33071090, 20.0, 18),
(25, 'Personal deodorants and anti-perspirants', 33072000, 20.0, 18),
(26, 'Air Freshner', 33074900, 20.0, 18),
(27, 'Hair Removal Cream', 33079010, 20.0, 18),
(28, 'Bath & Bathing Accessories', 33079090, 20.0, 18),
(29, 'Soap', 34011190, 0.0, 0),
(30, 'Cleaner', 34029049, 0.0, 0),
(31, 'Polish, Cream and Similar Preparations for footwear', 34051000, 0.0, 0),
(32, 'Prepared glues and other prepared Adhesive', 35061000, 10.0, 18),
(33, 'Screen Protector', 39199090, 15.0, 18),
(34, 'Carboys, bottles, flasks and similar articles', 39233010, 15.0, 18),
(35, 'Kitchenware', 39241090, 15.0, 18),
(36, 'Toilet Articles', 39249010, 15.0, 18),
(37, 'Rubber Contraceptives, Male', 40141010, 10.0, 18),
(38, 'Hot Water Bottles', 40149010, 10.0, 18),
(39, 'Breast Pump Accessories', 40149030, 10.0, 18),
(40, 'Feeding Bottle Nipples', 40149030, 10.0, 18),
(41, 'Floor Rugs', 40169100, 10.0, 18),
(42, 'Eraser', 40169200, 10.0, 18),
(43, 'Diaper Bags', 42021120, 15.0, 18),
(44, 'Makeup Bag', 42022240, 15.0, 18),
(45, 'Backpack, Laptob Bag and Bags', 42022290, 15.0, 18),
(46, 'Wallet and Purses', 42023120, 15.0, 18),
(47, 'Mobile Cases & Covers', 42029900, 15.0, 18),
(48, 'Belts', 42033000, 10.0, 18),
(49, 'Cutting Boards', 44191900, 10.0, 12),
(50, 'Parts of domestic Decorative articles used as tableware and kitchenware', 44219160, 10.0, 18),
(51, 'CARTONS, BOXES, CASES, BAGS AND OTHER PACKING CONTAINERS', 48191090, 10.0, 18),
(52, 'Decorative laminates', 48239019, 10.0, 18),
(53, 'Books', 49011010, 10.0, 5),
(54, 'Carpets, carpetting, rugs, mats and mattings', 57050090, 20.0, 12),
(55, 'MEN\'S OR BOY\'S OVERCOATS, CARCOATS, CAPES, CLOAKS, ANORAKS (INCLUDING SKI-JACKETS), WIND-CHEATERS, WINDJACKETS AND SIMILARARTICLES, KNITTED OR CROCHETED, OTHER THAN THOSE OF HEADING 6103', 61019090, 20.0, 12),
(56, 'Men Undergarments', 61071990, 20.0, 12),
(57, 'Baby Garments and Clothing Accessories', 61119090, 20.0, 12),
(58, 'Shirt', 62059090, 20.0, 12),
(59, 'Women Dresses', 62111200, 20.0, 12),
(60, 'Women Undergarments', 62129090, 20.0, 12),
(61, 'Curtain Drapes', 63039990, 10.0, 12),
(62, 'Cloth Face Mask', 63071090, 25.0, 12),
(63, 'Shoes', 64035111, 35.0, 18),
(64, 'Helmet', 65061090, 10.0, 18),
(65, 'Hair Wig', 67041990, 10.0, 18),
(66, 'IMITATION JEWELLERY', 71179090, 20.0, 3),
(67, 'Pan', 73239110, 20.0, 18),
(68, 'Pans of Cast Iron', 73239200, 20.0, 18),
(69, 'Cookware', 73239390, 20.0, 18),
(70, 'Storage Units', 73239990, 20.0, 18),
(71, 'Bakeware Set', 76151090, 20.0, 18),
(72, 'Can or cork openers', 82055110, 10.0, 18),
(73, 'Hand Tools', 82055190, 10.0, 18),
(74, 'Nail cutters', 82142010, 10.0, 18),
(75, 'Forks', 82159900, 10.0, 12),
(76, 'Clips', 83059020, 20.0, 18),
(77, 'Statuettes', 83062190, 20.0, 12),
(78, 'Keychain', 83089099, 10.0, 18),
(79, 'Industrial & Scientific', 84389090, 7.5, 18),
(80, 'Printers', 84433290, 10.0, 18),
(81, 'Office Or School Supplies', 84439951, 10.0, 18),
(82, 'Sewing Machine Articles', 84521019, 10.0, 18),
(83, 'Punching Machine', 84624910, 7.5, 18),
(84, 'Electronic Calculator', 84701000, 0.0, 18),
(85, 'Scanner', 84716050, 0.0, 18),
(86, 'Mouse', 84716060, 0.0, 18),
(87, 'Motherboard', 84733020, 0.0, 18),
(88, 'Graphics Tablets', 84733092, 0.0, 18),
(89, 'Computer Case Fan', 84733099, 0.0, 18),
(90, 'Computers', 84713099, 0.0, 18),
(91, 'Laptops', 84710099, 0.0, 18),
(92, 'Charger', 85044030, 20.0, 18),
(93, 'Adapters', 85044090, 20.0, 18),
(94, 'Computer Power Supply', 85049090, 15.0, 18),
(95, 'Shaver', 85101000, 20.0, 18),
(96, 'Hair clippers', 85102000, 20.0, 18),
(97, 'Hair Dressing Apparatus', 85163200, 20.0, 18),
(98, 'Other ovens; cookers, cooking plates, boiling rings, grillers and roasters', 85166000, 20.0, 18),
(99, 'Mobile Phones', 85171290, 20.0, 18),
(100, 'Modems ', 85176230, 0.0, 18),
(101, 'Routers', 85176290, 20.0, 18),
(102, 'Speakers', 85182900, 15.0, 18),
(103, 'Headphones & Microphones', 85183000, 15.0, 18),
(104, 'Streaming Device', 85219090, 20.0, 18),
(105, 'DVD', 85234160, 10.0, 18),
(106, 'Audio CD', 85234910, 10.0, 18),
(107, 'Memory Card', 85235220, 0.0, 18),
(108, 'Video Camera Recorders-digital camera', 85258020, 20.0, 18),
(109, 'Video Camera Recorders', 85258030, 20.0, 18),
(110, 'Computer Monitor', 85285900, 10.0, 28),
(111, 'Audio Switcher', 85437029, 7.5, 18),
(112, 'Cable', 85444299, 15.0, 18),
(113, 'Auto Parts', 87089900, 15.0, 28),
(114, 'Baby Carraiges and parts there of', 87150010, 10.0, 18),
(115, 'Sunglasses', 90041000, 20.0, 18),
(116, 'Binoculars', 90051000, 10.0, 18),
(117, 'Telescopes', 90059090, 10.0, 18),
(118, 'Parts and Accessories for Camera', 90069100, 10.0, 18),
(119, 'Projection screens', 90106000, 10.0, 18),
(120, 'Microscope', 90129000, 7.5, 18),
(121, 'Scissors', 90189022, 7.5, 18),
(122, 'WRIST-WATCHES, POCKET-WATCHES AND OTHER WATCHES,', 91011100, 20.0, 18),
(123, 'Smart Watch', 91029990, 20.0, 18),
(124, 'CLOCKS WITH WATCH MOVEMENTS,', 91039000, 20.0, 18),
(125, 'WATCH STRAPS, WATCH BANDS AND WATCH BRACELETS, AND PARTS THEREOF', 91131000, 10.0, 18),
(126, 'Musical Instruments Parts', 92099900, 10.0, 18),
(127, 'Cabinetware', 94033010, 25.0, 18),
(128, 'Furniture of Other Material', 94036000, 25.0, 18),
(129, 'Furniture', 94039000, 25.0, 18),
(130, 'MATTRESS SUPPORTS; ARTICLES OF BEDDING ND SIMILAR FURNISHING', 94041000, 25.0, 18),
(131, 'Lamps & Lighting Fittings', 94051090, 25.0, 18),
(132, 'Toy', 95030090, 60.0, 18),
(133, 'Video Game Consoles and Machine', 95045000, 20.0, 28),
(134, 'Decorations', 95059090, 20.0, 18),
(135, 'ARTICLES AND EQUIPMENT FOR GENERAL PHYSICAL EXERCISE,', 95061100, 20.0, 18),
(136, 'Tennis Rackets', 95065100, 20.0, 18),
(137, 'Football', 95066210, 20.0, 18),
(138, 'Basketball', 95066230, 20.0, 18),
(139, 'Golf', 95066930, 20.0, 18),
(140, 'Roller Skater', 95067000, 20.0, 18),
(141, 'Sports net', 95069990, 20.0, 18),
(142, 'Fishing Rod , Fishing Hook', 95079090, 20.0, 12),
(143, 'Toothbrushes', 96032100, 20.0, 18),
(144, 'Brushes for the Application of Cosmetic', 96033020, 20.0, 18),
(145, 'Grooming Brush', 96033090, 20.0, 18),
(146, 'Pen', 96081019, 10.0, 18),
(147, 'Marker Pen', 96082000, 10.0, 18),
(148, 'Stylus', 96083099, 10.0, 18),
(149, 'Pencil Crayons and Pastels', 96099090, 10.0, 12),
(150, 'COMBS, HAIR-SLIDES AND THE LIKE, HAIRPINS, CURLING PINS, CURLING GRIPS, HAIR-CURLERS AND THE LIKE, OTHER THAN THOSE OF HEADING 8516,AND PARTS THEREOF', 96151100, 20.0, 12),
(151, 'Casserol and other vacuum containers', 96170013, 20.0, 18),
(152, 'Sanitary Napkin', 96190010, 10.0, 0),
(153, 'Tampons', 96190020, 10.0, 0),
(154, 'Napkins and napkin liners for babies', 96190030, 10.0, 12),
(155, 'Clinical diapers', 96190040, 10.0, 12),
(156, 'Cloth Diapers', 96190090, 10.0, 12);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
