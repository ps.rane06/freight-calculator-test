<?php
class Database
{
    private static $conn;

    public static function getConnection($env)
    {
        if (!self::$conn) {
            try {
                self::$conn = new mysqli($env['DATABASE_HOSTNAME'], $env['DATABASE_USERNAME'], $env['DATABASE_PASSWORD'], $env['DATABASE_NAME']);
                if (self::$conn->connect_error) {
                    throw new Exception("Database connection failed: " . self::$conn->connect_error);
                }
            } catch (Exception $e) {
                // Handle connection error (log, display, etc.)
                die("Database connection error: " . $e->getMessage());
            }
        }
        return self::$conn;
    }
}